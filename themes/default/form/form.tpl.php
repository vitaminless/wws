<form id="<?=$args['id']?>" name="<?=$args['name']?>" action="<?=$args['action']?>" method="<?=$args['method']?>" <?=html_args($args['args'])?>>
	<div class="messages">
		<?php foreach ($args['messages'] as $type=>$messages): ?>
		<?php if (!empty($messages)): ?>
		<ul class="<?=$type?>">
			<?php foreach ($messages as $message): ?>
			<li><p><?=$message?></p></li>
			<?php endforeach; ?>
		</ul>
		<?php endif; ?>
		<?php endforeach; ?>
	</div>
	<?=$args['fields']?>
</form>

