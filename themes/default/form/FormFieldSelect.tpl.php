<div <?=html_args($args['args'])?>>
	
	<div class="f30l">
		<div class="wrapper">
			<label for="<?=$args['id']?>"><?=$args['label']?></label>
		</div>
	</div>
	<div class="f70r">
		<div class="wrapper">
			<select id="<?=$args['id']?>" name="<?=$args['name']?>">
				<?=$args['options']?>
			</select>
		</div>
	</div>
	<div class="clear">&nbsp;</div>
	
	<div class="messages">
		<?php foreach ($args['messages'] as $type=>$messages): ?>
		<?php if (!empty($messages)): ?>
		<ul class="<?=$type?>">
			<?php foreach ($messages as $message): ?>
			<li><p><?=$message?></p></li>
			<?php endforeach; ?>
		</ul>
		<?php endif; ?>
		<?php endforeach; ?>
	</div>
	
</div>

