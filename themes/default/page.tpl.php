<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	
	<head>
		<title><?=$this->args['name']?></title>
		<link rel="stylesheet" type="text/css" href="/themes/default/css/style.css" />
		<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
	</head>
	
	<body>
		<div id="page">
			<div id="page_margins">
				
				<div id="head">
					<h1><?=l($this->args['name'], '/')?></h1>
				</div>
				
				<div id="nav">
					<ul>
						<li><?=l('&Uuml;berblick', '/')?></li>
						<li><?=l('Kategorien', '/category')?></li>
						<li><?=l('Artikel', '/article')?></li>
					</ul>
				</div>
				
				<div id="debug">
					<?=Page::$DEBUG?>
				</div>
				
				<div class="messages">
					<?php foreach ($this->args['messages'] as $type=>$messages): ?>
					<?php if (!empty($messages)): ?>
					<ul class="<?=$type?>">
						<?php foreach ($messages as $message): ?>
						<li><p><?=$message?></p></li>
						<?php endforeach; ?>
					</ul>
					<?php endif; ?>
					<?php endforeach; ?>
				</div>
				
				<div id="content">
					<h2><?=$this->args['title']?></h2>
					<?=$this->args['content']?>
				</div>
				
				<div id="foot">
					<p>&copy; Benjamin R&ouml;sler 2014</p>
				</div>
				
			</div>
		</div>
	</body>

</html>
