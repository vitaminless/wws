<?php

/**
 * Search.class.php
 * @author Benjamin Rösler
 * @version 0.1
 **/

class Search extends Module {
	
	protected $dir = __DIR__;
	
	public function __construct(){
		parent::__construct();
	}
	
	public function search(){
		$query = $_REQUEST['query'];
		$type = $_REQUEST['type'];
		$result = ($type=='article')?($this->searchArticle($query)):($this->searchCategory($query));
		$json = array(
			'type' => 'json',
			'content' => $result
		);
		Page::GET_INSTANCE()->page200($json);
	}
	
	private function searchCategory($query){
		$result = Page::GET_INSTANCE()->db->query('category', array(), 'name LIKE "%' . $query . '%"');
		$table = array(
			'head' => array(
				'row' => array(
					'args' => '',
					'cells' => array(
						array('value' => 'ID', 'args' => ''),
						array('value' => 'Name', 'args' => ''),
						array('value' => 'Artikel', 'args' => ''),
						array('value' => '', 'args' => ''),
						array('value' => '', 'args' => '')
					)
				)
			),
			'body' => array('rows' => array(), 'args' => ''),
			'args' => ''
		);
		foreach ($result as $item){
			$articles = Page::GET_INSTANCE()->db->query('article', array('id'), 'category=' . $item['id']);
			$table['body']['rows'][] = array(
				'cells' => array(
					array('value' => $item['id'], 'args' => 'style="text-align: right;"'),
					array('value' => l($item['name'], '/category/show/' . $item['id']), 'args' => ''),
					array('value' => count($articles), 'args' => 'style="text-align: right;"'),
					array('value' => l('bearbeiten', '/category/edit/' . $item['id']), 'args' => 'style="text-align: center;"'),
					array('value' => l('l&ouml;schen', '/category/remove/' . $item['id']), 'args' => 'style="text-align: center;"')
				)
			);
		}
		return $table;
	}
	
	private function searchArticle($query){
		$result = Page::GET_INSTANCE()->db->query('article', array('*', '(SELECT name FROM category WHERE id=article.category) AS category', '(SELECT id FROM category WHERE id=article.category) AS cid'), 'art_id LIKE "%' . $query . '%" OR description LIKE "%' . $query . '%"');
		$table = array(
			'head' => array(
				'row' => array(
					'cells' => array(
						array('value' => 'ID', 'args' => ''),
						array('value' => 'Kategorie', 'args' => ''),
						array('value' => 'Nummer', 'args' => ''),
						array('value' => 'Beschreibung', 'args' => ''),
						array('value' => 'EK-Preis', 'args' => ''),
						array('value' => 'Arbeitszeit', 'args' => ''),
						array('value' => 'Mindestlohn', 'args' => ''),
						array('value' => '', 'args' => ''),
						array('value' => '', 'args' => ''),
						array('value' => '', 'args' => '')
					),
					'args' => ''
				),
				'args' => ''
			),
			'body' => array('rows' => array(), 'args' => ''),
			'args' => ''
		);
		foreach ($result as $item){
			$table['body']['rows'][] = array(
				'cells' => array(
					array('value' => $item['id'], 'args' => 'style="text-align: right;"'),
					array('value' => l($item['category'], '/category/show/' . $item['cid']), 'args' => ''),
					array('value' => $item['art_id'], 'args' => 'style="text-align: right;"'),
					array('value' => $item['description'], 'args' => ''),
					array('value' => $item['base_price'], 'args' => 'style="text-align: right;"'),
					array('value' => $item['minutes'], 'args' => 'style="text-align: right;"'),
					array('value' => $item['wage'], 'args' => 'style="text-align: right;"'),
					array('value' => l('anzeigen', '/article/show/' . $item['id']), 'args' => 'style="text-align: center;"'),
					array('value' => l('bearbeiten', '/article/edit/' . $item['id']), 'args' => 'style="text-align: center;"'),
					array('value' => l('l&ouml;schen', '/article/remove/' . $item['id']), 'args' => 'style="text-align: center;"')
				),
				'args' => ''
			);
		}
		return $table;
	}
	
}

?>
