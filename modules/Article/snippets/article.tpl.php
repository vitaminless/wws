<div class="f30l">
	<div class="wrapper">
		<h4>ID</h4>
	</div>
</div>
<div class="f70r">
	<div class="wrapper">
		<p><?=$args['id']?></p>
	</div>
</div>
<div class="clear">&nbsp;</div>

<div class="f30l">
	<div class="wrapper">
		<h4>Kategorie</h4>
	</div>
</div>
<div class="f70r">
	<div class="wrapper">
		<p><?=l($args['category'], '/category/show/' . $args['cid'])?></p>
	</div>
</div>
<div class="clear">&nbsp;</div>

<div class="f30l">
	<div class="wrapper">
		<h4>Nummer</h4>
	</div>
</div>
<div class="f70r">
	<div class="wrapper">
		<p><?=$args['art_id']?></p>
	</div>
</div>
<div class="clear">&nbsp;</div>

<div class="f30l">
	<div class="wrapper">
		<h4>Beschreibung</h4>
	</div>
</div>
<div class="f70r">
	<div class="wrapper">
		<p><?=$args['description']?></p>
	</div>
</div>
<div class="clear">&nbsp;</div>

<div class="f30l">
	<div class="wrapper">
		<h4>EK-Preis</h4>
	</div>
</div>
<div class="f70r">
	<div class="wrapper">
		<p><?=$args['base_price']?></p>
	</div>
</div>
<div class="clear">&nbsp;</div>

<div class="f30l">
	<div class="wrapper">
		<h4>Arbeitszeit</h4>
	</div>
</div>
<div class="f70r">
	<div class="wrapper">
		<p><?=$args['minutes']?></p>
	</div>
</div>
<div class="clear">&nbsp;</div>

<div class="f30l">
	<div class="wrapper">
		<h4>Mindestlohn</h4>
	</div>
</div>
<div class="f70r">
	<div class="wrapper">
		<p><?=$args['wage']?></p>
	</div>
</div>
<div class="clear">&nbsp;</div>

