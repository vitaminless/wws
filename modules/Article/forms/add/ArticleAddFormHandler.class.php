<?php

/**
 * ArticleAddFormHandler.class.php
 * @author Benjamin Rösler
 * @version 0.1
 **/

class ArticleAddFormHandler extends FormHandler {
	
	public function validate($form, $values){
		foreach ($values as $val){
			if (empty($val)){
				$form->addMessage(MSG_ERROR, 'Alle Felder m&uuml;ssen ausgef&uuml;llt werden!');
				return false;
			}
		}
		return true;
	}
	
	public function execute($values){
		$btn = $values['buttons'];
		unset($values['buttons']);
		$values['base_price'] = $this->str2float($values['base_price']);
		$values['minutes'] = $this->str2float($values['minutes']);
		$values['wage'] = $this->str2float($values['wage']);
		$id = Page::GET_INSTANCE()->db->insert('article', $values);
		if (isset($btn['continue']))
			Page::GET_INSTANCE()->redirect('/article/add');
		else
			Page::GET_INSTANCE()->redirect('/article/show/' . $id);
	}
	
	private function str2float($val){
		$val = str_replace(',', '.', $val);
		return floatval($val);
	}
	
}

?>
