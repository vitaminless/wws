<?php

require_once 'ArticleAddFormHandler.class.php';

$categories = array();
$fcats = Page::GET_INSTANCE()->db->query('category', array(), null, 'name ASC');
foreach ($fcats as $fcat) $categories[$fcat['id']] = $fcat['name'];

$form = array(
	'name' => 'add_article',
	'handler' => new ArticleAddFormHandler(),
	'fields' => array(
		'art_id' => array(
			'type' => 'FormFieldText',
			'label' => 'Artikelnummer',
			'args' => array(
				'class' => array('required')
			)
		),
		'description' => array(
			'type' => 'FormFieldText',
			'label' => 'Beschreibung',
			'args' => array(
				'class' => array('required')
			)
		),
		'category' => array(
			'type' => 'FormFieldSelect',
			'label' => 'Kategorie',
			'options' => $categories,
			'args' => array(
				'class' => array('required')
			)
		),
		'base_price' => array(
			'type' => 'FormFieldText',
			'label' => 'Einkaufspreis',
			'args' => array(
				'class' => array('required')
			)
		),
		'minutes' => array(
			'type' => 'FormFieldText',
			'label' => 'Arbeitszeit',
			'args' => array(
				'class' => array('required')
			)
		),
		'wage' => array(
			'type' => 'FormFieldText',
			'label' => 'Mindestlohn',
			'args' => array(
				'class' => array('required')
			)
		),
		'buttons' => array(
			'type' => 'FormFieldButtonGroup',
			'buttons' => array(
				'save' => array(
					'type' => 'FormFieldButton',
					'label' => 'Speichern'
				),
				'continue' => array(
					'type' => 'FormFieldButton',
					'label' => 'Speichern und weiteren anlegen'
				)
			)
		)
	)
);

?>
