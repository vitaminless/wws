<?php

require_once 'EditArticleFormHandler.class.php';

$categories = array();
$fcats = Page::GET_INSTANCE()->db->query('category', array(), null, 'name ASC');
foreach ($fcats as $fcat) $categories[$fcat['id']] = $fcat['name'];

$form = array(
	'name' => 'add_article',
	'handler' => new EditArticleFormHandler(),
	'fields' => array(
		'id' => array(
			'type' => 'FormFieldHidden',
			'value' => $article['id']
		),
		'art_id' => array(
			'type' => 'FormFieldText',
			'label' => 'Artikelnummer',
			'value' => $article['art_id'],
			'args' => array(
				'class' => array('required')
			)
		),
		'description' => array(
			'type' => 'FormFieldText',
			'label' => 'Beschreibung',
			'value' => $article['description'],
			'args' => array(
				'class' => array('required')
			)
		),
		'category' => array(
			'type' => 'FormFieldSelect',
			'label' => 'Kategorie',
			'options' => $categories,
			'value' => $article['category'],
			'args' => array(
				'class' => array('required')
			)
		),
		'base_price' => array(
			'type' => 'FormFieldText',
			'label' => 'Einkaufspreis',
			'value' => str_replace('.', ',', $article['base_price']),
			'args' => array(
				'class' => array('required')
			)
		),
		'minutes' => array(
			'type' => 'FormFieldText',
			'label' => 'Arbeitszeit',
			'value' => str_replace('.', ',', $article['minutes']),
			'args' => array(
				'class' => array('required')
			)
		),
		'wage' => array(
			'type' => 'FormFieldText',
			'label' => 'Mindestlohn',
			'value' => str_replace('.', ',', $article['wage']),
			'args' => array(
				'class' => array('required')
			)
		),
		'buttons' => array(
			'type' => 'FormFieldButtonGroup',
			'buttons' => array(
				'save' => array(
					'type' => 'FormFieldButton',
					'label' => 'Speichern'
				)
			)
		)
	)
);

?>
