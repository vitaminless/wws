<?php

/**
 * EditArticleFormHandler.class.php
 * @author Benjamin Rösler
 * @version 0.1
 **/

class EditArticleFormHandler extends FormHandler {
	
	public function validate($form, $values){
		foreach ($values as $value){
			if (empty($value)){
				$form->addMessage(MSG_ERROR, 'Alle Felder m&uuml;ssen ausgef&uuml;llt werden!');
				return false;
			}
		}
		return true;
	}
	
	public function execute($values){
		unset($values['buttons']);
		$values['base_price'] = $this->str2float($values['base_price']);
		$values['minutes'] = $this->str2float($values['minutes']);
		$values['wage'] = $this->str2float($values['wage']);
		Page::GET_INSTANCE()->db->update('article', $values, 'id=' . $values['id']);
		Page::GET_INSTANCE()->redirect('/article/show/' . $values['id']);
	}
	
	private function str2float($val){
		$val = str_replace(',', '.', $val);
		return floatval($val);
	}
	
}

?>
