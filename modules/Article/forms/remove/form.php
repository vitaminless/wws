<?php

require_once 'RemoveArticleFormHandler.class.php';

$form = array(
	'name' => 'delete_article',
	'handler' => new RemoveArticleFormHandler(),
	'fields' => array(
		'id' => array(
			'type' => 'FormFieldHidden',
			'value' => $args[2]
		),
		'buttons' => array(
			'type' => 'FormFieldButtonGroup',
			'buttons' => array(
				'yes' => array(
					'type' => 'FormFieldButton',
					'label' => 'Ja'
				),
				'no' => array(
					'type' => 'FormFieldButton',
					'label' => 'Nein'
				)
			)
		)
	)
);

?>
