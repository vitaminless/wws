<?php

/**
 * RemoveArticleFormHandler.class.php
 * @author Benjamin Rösler
 * @version 0.1
 **/

class RemoveArticleFormHandler extends FormHandler {
	
	public function validate($form, $values){
		return true;
	}
	
	public function execute($values){
		if (isset($values['buttons']['yes']))
			Page::GET_INSTANCE()->db->remove('article', 'id=' . $values['id']);
		Page::GET_INSTANCE()->redirect('/article');
	}
	
}

?>
