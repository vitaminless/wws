CREATE TABLE article (
	id INT AUTO_INCREMENT PRIMARY KEY,
	category INT,
	art_id VARCHAR(64) NOT NULL,
	description TEXT NOT NULL,
	base_price DECIMAL(64, 2),
	minutes DECIMAL(64, 2),
	wage DECIMAL(64, 2),
	FOREIGN KEY (category) REFERENCES Category(id) ON UPDATE CASCADE ON DELETE SET NULL
);

