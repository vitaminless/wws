<?php

/**
 * Article.class.php
 * @author Benjamin Rösler
 * @version 0.1
 **/

class Article extends Module {
	
	protected $dir = __DIR__;
	
	public function __construct(){
		parent::__construct();
	}
	
	public function ls(){
		$table = array(
			'head' => array(
				'row' => array(
					'cells' => array(
						array('value' => 'ID'),
						array('value' => 'Kategorie'),
						array('value' => 'Nummer'),
						array('value' => 'Beschreibung'),
						array('value' => 'EK-Preis'),
						array('value' => 'Arbeitszeit'),
						array('value' => 'Mindestlohn'),
						array('value' => ''),
						array('value' => ''),
						array('value' => '')
					)
				)
			),
			'body' => array('rows' => array())
		);
		$articles = Page::GET_INSTANCE()->db->query('article', array('*', '(SELECT name FROM category WHERE id=article.category) AS category'));
		foreach ($articles as $article){
			$table['body']['rows'][] = array(
				'cells' => array(
					array('value' => $article['id'], 'args' => array('style' => 'text-align: right;')),
					array('value' => $article['category']),
					array('value' => $article['art_id'], 'args' => array('style' => 'text-align: right;')),
					array('value' => $article['description']),
					array('value' => $article['base_price'], 'args' => array('style' => 'text-align: right;')),
					array('value' => $article['minutes'], 'args' => array('style' => 'text-align: right;')),
					array('value' => $article['wage'], 'args' => array('style' => 'text-align: right;')),
					array('value' => l('anzeigen', '/article/show/' . $article['id']), 'args' => array('style' => 'text-align: center;')),
					array('value' => l('bearbeiten', '/article/edit/' . $article['id']), 'args' => array('style' => 'text-align: center;')),
					array('value' => l('l&ouml;schen', '/article/remove/' . $article['id']), 'args' => array('style' => 'text-align: center;'))
				)
			);
		}
		$page = array(
			'type' => 'html',
			'title' => 'Artikel',
			'content' => array(
				'<div style="text-align: right;">' . l('+ Neu', '/article/add') . '</div>',
				new HtmlTable($table),
				'<div style="text-align: right;">' . l('+ Neu', '/article/add') . '</div>',
			)
		);
		Page::GET_INSTANCE()->page200($page);
	}
	
	public function show(){
		$args = explode('/', $_REQUEST['q']);
		$article = Page::GET_INSTANCE()->db->query('article', array('*', '(SELECT name FROM category WHERE id=article.category) AS category', 'category AS cid'), 'id=' . $args[2]);
		if (empty($article)){
			Page::GET_INSTANCE()->page404(array(
				'type' => 'html',
				'title' => '404 Kein Artikel mit der ID ' . $args[2],
				'content' => 'Es exsistiert kein Artikel mit der ID ' . $args[2] . '!'
			));
		} else {
			$article = $article[0];
		}
		
		$page = array(
			'type' => 'html',
			'title' => 'Artikel "' . $article['art_id'] . '"',
			'content' => return_include($this->dir . '/snippets/article.tpl.php', $article)
		);
		Page::GET_INSTANCE()->page200($page);
	}
	
	public function add(){
		include 'forms/add/form.php';
		$page = array(
			'type' => 'html',
			'title' => 'Neuer Artikel',
			'content' => new Form($form)
		);
		Page::GET_INSTANCE()->page200($page);
	}
	
	public function edit(){
		$args = explode('/', $_REQUEST['q']);
		$article = Page::GET_INSTANCE()->db->query('article', array(), 'id=' . $args[2]);
		if (empty($article)){
			Page::GET_INSTANCE()->page404(array(
				'type' => 'html',
				'title' => '404 Kein Artikel mit der ID ' . $args[2],
				'content' => 'Es exsistiert kein Artikel mit der ID ' . $args[2] . '!'
			));
		} else {
			$article = $article[0];
		}
		include 'forms/edit/form.php';
		$page = array(
			'type' => 'html',
			'title' => 'Bearbeite Artikel',
			'content' => new Form($form)
		);
		Page::GET_INSTANCE()->page200($page);
	}
	
	public function remove(){
		$args = explode('/', $_REQUEST['q']);
		$art_id = Page::GET_INSTANCE()->db->query('article', array('art_id'), 'id=' . $args[2]);
		if (empty($art_id)){
			Page::GET_INSTANCE()->page404(array(
				'type' => 'html',
				'title' => '404 Kein Artikel mit der ID ' . $args[2],
				'content' => 'Es exsistiert kein Artikel mit der ID ' . $args[2] . '!'
			));
		} else {
			$art_id = $art_id[0]['art_id'];
		}
		include 'forms/remove/form.php';
		$page = array(
			'type' => 'html',
			'title' => 'L&ouml;sche Artikel',
			'content' => array(
				'Wollen Sie den Artikel "' . $art_id . '" l&ouml;schen?',
				new Form($form)
			)
		);
		Page::GET_INSTANCE()->page200($page);
	}
	
}

?>
