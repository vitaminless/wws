<?php

/**
 * Overview.class.php
 * @author Benjamin Rösler
 * @verison 0.1
 **/

class Overview extends Module {
	
	protected $dir = __DIR__;
	
	public function __construct(){
		parent::__construct();
	}
	
	public function overview(){
		$category = return_include($this->dir . '/snippets/category.tpl.php', null);
		$article = return_include($this->dir . '/snippets/article.tpl.php', null);
		$search = return_include($this->dir . '/snippets/search.tpl.php', null);
		$page = array(
			'type' => 'html',
			'title' => '&Uuml;berblick',
			'content' => array(
				$category,
				$article,
				'<div class="clear">&nbsp;</div>',
				$search
			)
		);
		Page::GET_INSTANCE()->page200($page);
	}
	
}

?>
