<?php

/**
 * article.tpl.php
 * @author Benjamin Rösler
 * @version 0.1
 **/

$categories = Page::GET_INSTANCE()->db->query('category', array('COUNT(id) AS count'));
?>
<div id="category_block" class="block f50l">
	<div>
		<h3>Kategorien (<?=$categories[0]['count']?>)</h3>
		<div>
			<ul>
				<li><a href="/category">Liste</a></li>
				<li><a href="/category/add">Neu</a></li>
			</ul>
		</div>
	</div>
</div>

