<div id="article_block" class="block">
	<div>
		<h3>Suche</h3>
		<div>
			<form id="search" name="search" action="/json/search" method="post">
				<input type="text" id="query" name="query" style="width: 70%; display: inline-block;" />
				<select name="type" id="type" style="width: 20%; display: inline-block;">
					<option value="article">Artikel</option>
					<option value="category">Kategorie</option>
				</select>
				<button>Suchen</button>
			</form>
		</div>
	</div>
</div>

<div id="search_result" style="margin: 0.5em 1em 0 1em;"></div>

<script type="text/javascript">
$('#search').submit(function(ev){
	ev.preventDefault();
	var url = $(this).attr('action');
	$.post(
		url,
		{ query: $('#query').val(), type: $('#type').val() },
		function(data, state, xhr){
			data = $.parseJSON(data);
			console.log(data);
			var table = '<table ' + data.args + '>';
			table += '<thead ' + data.head.args + '>'
			table += '<tr ' + data.head.row.args + '>';
			for (var i=0; i<data.head.row.cells.length; i++){
				table += '<th ' + data.head.row.cells[i].args + '>';
				table += data.head.row.cells[i].value;
				table += '</th>';
			}
			table += '</tr>';
			table += '</thead>';
			table += '<tbody ' + data.body.args + '>';
			for (var i=0; i<data.body.rows.length; i++){
				table += '<tr ' + data.body.rows[i].args + '>';
				for (var j=0; j<data.body.rows[i].cells.length; j++){
					table += '<td ' + data.body.rows[i].cells[j].args + '>';
					table += data.body.rows[i].cells[j].value;
					table += '</td>';
				}
				table += '</tr>';
			}
			table += '</tbody>';
			table += '</table>';
			$('#search_result').html(table);
		}
	);
});
</script>
