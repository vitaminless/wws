<?php

/**
 * article.tpl.php
 * @author Benjamin Rösler
 * @version 0.1
 **/

$articles = Page::GET_INSTANCE()->db->query('article', array('COUNT(id) AS count'));

?>
<div id="article_block" class="block f50r">
	<div>
		<h3>Artikel (<?=$articles[0]['count']?>)</h3>
		<div>
			<ul>
				<li><a href="/article">Liste</a></li>
				<li><a href="/article/add">Neu</a></li>
			</ul>
		</div>
	</div>
</div>

