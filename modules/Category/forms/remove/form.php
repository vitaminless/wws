<?php

require_once 'RemoveCategoryFormHandler.class.php';

$form = array(
	'name' => 'delete_category',
	'handler' => new RemoveCategoryFormHandler(),
	'fields' => array(
		'id' => array(
			'type' => 'FormFieldHidden',
			'value' => $args[2]
		),
		'buttons' => array(
			'type' => 'FormFieldButtonGroup',
			'buttons' => array(
				'yes' => array(
					'type' => 'FormFieldButton',
					'label' => 'Ja'
				),
				'no' => array(
					'type' => 'FormFieldButton',
					'label' => 'Nein'
				)
			)
		)
	)
);

?>
