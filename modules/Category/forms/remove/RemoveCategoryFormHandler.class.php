<?php

/**
 * RemoveCategoryFormHandler.class.php
 * @author Benjamin Rösler
 * @version 0.1
 **/

class RemoveCategoryFormHandler extends FormHandler {
	
	public function validate($form, $values){
		return true;
	}
	
	public function execute($values){
		if (isset($values['buttons']['yes']))
			Page::GET_INSTANCE()->db->remove('category', 'id=' . $values['id']);
		Page::GET_INSTANCE()->redirect('/category');
	}
	
}

?>
