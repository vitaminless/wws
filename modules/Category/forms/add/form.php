<?php

require_once 'AddCategoryFormHandler.class.php';

$form = array(
	'name' => 'add_category',
	'handler' => new AddCategoryFormHandler(),
	'fields' => array(
		'name' => array(
			'type' => 'FormFieldText',
			'label' => 'Name',
			'args' => array(
				'class' => array('required')
			)
		),
		'buttons' => array(
			'type' => 'FormFieldButtonGroup',
			'buttons' => array(
				'save' => array(
					'type' => 'FormFieldButton',
					'label' => 'Speichern'
				),
				'continue' => array(
					'type' => 'FormFieldButton',
					'label' => 'Speichern und weiteren anlegen'
				)
			)
		)
	),
);

?>
