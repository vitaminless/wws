<?php

/**
 * AddCategoryFormHandler.class.php
 * @author Benjamin Rösler
 * @version 0.1
 **/

class AddCategoryFormHandler extends FormHandler {
	
	public function validate($form, $values){
		if (empty($values['name'])){
			$form->addMessage(MSG_ERROR, 'Die Kategorie muss einen Namen haben!');
			return false;
		}
		return true;
	}
	
	public function execute($values){
		$btn = $values['buttons'];
		unset($values['buttons']);
		$id = Page::GET_INSTANCE()->db->insert('category', $values);
		if (isset($btn['continue']))
			Page::GET_INSTANCE()->redirect('/category/add');
		else
			Page::GET_INSTANCE()->redirect('/category/show/' . $id);
	}
	
}

?>
