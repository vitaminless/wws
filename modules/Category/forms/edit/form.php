<?php

require_once 'EditCategoryFormHandler.class.php';

$form = array(
	'name' => 'add_category',
	'handler' => new EditCategoryFormHandler(),
	'fields' => array(
		'id' => array(
			'type' => 'FormFieldHidden',
			'value' => $category['id']
		),
		'name' => array(
			'type' => 'FormFieldText',
			'label' => 'Name',
			'value' => $category['name'],
			'args' => array(
				'class' => array('required')
			)
		),
		'buttons' => array(
			'type' => 'FormFieldButtonGroup',
			'buttons' => array(
				'save' => array(
					'type' => 'FormFieldButton',
					'label' => 'Speichern'
				)
			)
		)
	),
);

?>
