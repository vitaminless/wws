<?php

/**
 * EditCategoryFormHandler.class.php
 * @author Benjamin Rösler
 * @version 0.1
 **/

class EditCategoryFormHandler extends FormHandler {
	
	public function validate($form, $values){
		foreach ($values as $value){
			if (empty($value)){
				$form->addMessage(MSG_ERROR, 'Alle Felder m&uuml;ssen ausgef&uuml;llt werden!');
				return false;
			}
		}
		return true;
	}
	
	public function execute($values){
		$btn = $values['buttons'];
		unset($values['buttons']);
		Page::GET_INSTANCE()->db->update('category', $values, 'id=' . $values['id']);
		Page::GET_INSTANCE()->redirect('/category/show/' . $values['id']);
	}
	
}

?>
