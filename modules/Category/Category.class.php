<?php

/**
 * Category.class.php
 * @author Benjamin Rösler
 * @version 0.1
 **/

class Category extends Module {
	
	protected $dir = __DIR__;
	
	public function __construct(){
		parent::__construct();
	}
	
	public function ls(){
		$table = array(
			'head' => array(
				'row' => array(
					'cells' => array(
						array('value' => 'ID'),
						array('value' => 'Name'),
						array('value' => 'Artikel'),
						array('value' => ''),
						array('value' => '')
					)
				)
			),
			'body' => array('rows' => array())
		);
		$categories = Page::GET_INSTANCE()->db->query('category', array('id', 'name'), null, 'name ASC');
		foreach ($categories as $category){
			if (!empty($category['id'])){
				$art_count = Page::GET_INSTANCE()->db->query('article', array(), 'category=' . $category['id']);
				$category['art_count'] = count($art_count);
				$table['body']['rows'][] = array(
					'cells' => array(
						array('value' => $category['id'], 'args' => array('style' => 'text-align: right;')),
						array('value' => l($category['name'], '/category/show/' . $category['id'])),
						array('value' => $category['art_count'], 'args' => array('style' => 'text-align: right;')),
						array('value' => l('bearbeiten', '/category/edit/' . $category['id']), 'args' => array('style' => 'text-align: center;')),
						array('value' => l('l&ouml;schen', '/category/remove/' . $category['id']), 'args' => array('style' => 'text-align: center;'))
					)
				);
			}
		}
		$page = array(
			'type' => 'html',
			'title' => 'Kategorien',
			'content' => array(
				'<div style="text-align: right;">' . l('+ Neu', '/category/add', array('class' => array('button'))) . '</div>',
				new HtmlTable($table),
				'<div style="text-align: right;">' . l('+ Neu', '/category/add', array('class' => array('button'))) . '</div>'
			)
		);
		Page::GET_INSTANCE()->page200($page);
	}
	
	public function show(){
		$args = explode('/', $_REQUEST['q']);
		$category = Page::GET_INSTANCE()->db->query('category', array(), 'id=' . $args[2]);
		if (empty($category)){
			Page::GET_INSTANCE()->page404(array(
				'type' => 'html',
				'title' => '404 Keine Kategorie mit der ID ' . $args[2],
				'content' => 'Es exsistiert keine Kategorie mit der ID ' . $args[2] . '!'
			));
		} else {
			$category = $category[0];
		}
		
		$table = array(
			'head' => array(
				'row' => array(
					'cells' => array(
						array('value' => 'ID'),
						array('value' => 'Kategorie'),
						array('value' => 'Nummer'),
						array('value' => 'Beschreibung'),
						array('value' => 'EK-Preis'),
						array('value' => 'Arbeitszeit'),
						array('value' => 'Mindestlohn'),
						array('value' => ''),
						array('value' => ''),
						array('value' => '')
					)
				)
			),
			'body' => array('rows' => array())
		);
		$articles = Page::GET_INSTANCE()->db->query('article', array(), 'category=' . $args[2], 'id ASC');
		foreach ($articles as $article){
			$table['body']['rows'][] = array(
				'cells' => array(
					array('value' => $article['id']),
					array('value' => $article['category']),
					array('value' => $article['art_id']),
					array('value' => $article['description']),
					array('value' => $article['base_price']),
					array('value' => $article['minutes']),
					array('value' => $article['wage']),
					array('value' => l('anzeigen', '/article/show/' . $article['id'])),
					array('value' => l('bearbeiten', '/article/edit/' . $article['id'])),
					array('value' => l('l&ouml;schen', '/article/remove/' . $article['id']))
				)
			);
		}
		
		$page = array(
			'type' => 'html',
			'title' => 'Kategorie "' . $category['name'] . '"',
			'content' => array(
				new HtmlTable($table)
			)
		);
		Page::GET_INSTANCE()->page200($page);
	}
	
	public function add(){
		include 'forms/add/form.php';
		$page = array(
			'type' => 'html',
			'title' => 'Neue Kategorie',
			'content' => new Form($form)
		);
		Page::GET_INSTANCE()->page200($page);
	}
	
	public function edit(){
		$args = explode('/', $_REQUEST['q']);
		$category = Page::GET_INSTANCE()->db->query('category', array(), 'id=' . $args[2]);
		if (empty($category)){
			Page::GET_INSTANCE()->page404(array(
				'type' => 'html',
				'title' => '404 Keine Kategorie mit der ID ' . $args[2],
				'content' => 'Es exsistiert keine Kategorie mit der ID ' . $args[2] . '!'
			));
		} else {
			$category = $category[0];
		}
		include 'forms/edit/form.php';
		$page = array(
			'type' => 'html',
			'title' => 'Bearbeite Kategorie',
			'content' => new Form($form)
		);
		Page::GET_INSTANCE()->page200($page);
	}
	
	public function remove(){
		$args = explode('/', $_REQUEST['q']);
		include 'forms/remove/form.php';
		$category = Page::GET_INSTANCE()->db->query('category', array('name'), 'id=' . $args[2]);
		if (empty($category)){
			Page::GET_INSTANCE()->page404(array(
				'type' => 'html',
				'title' => '404 Keine Kategorie mit der ID ' . $args[2],
				'content' => 'Es exsistiert keine Kategorie mit der ID ' . $args[2] . '!'
			));
		} else {
			$category = $category[0];
		}
		$page = array(
			'type' => 'html',
			'title' => 'L&ouml;sche Kategorie',
			'content' => array(
				'<p>Wollen Sie wirklich die Kategorie "' . $category['name'] . '" l&ouml;schen, alle Artikel dieser Kategorie werden ebenfalls gel&ouml;scht?<p>',
				new Form($form)
			)
		);
		Page::GET_INSTANCE()->page200($page);
	}
	
}

?>
