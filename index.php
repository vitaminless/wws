<?php

/**
 * index.php
 * Launch application
 * @author Benjamin Rösler
 * @version 0.1
 **/

// fixing include search path
set_include_path(get_include_path() . ':' . __DIR__);

// correcting query if nessesary
if (!isset($_REQUEST['q']) || empty($_REQUEST['q']))
	$_REQUEST['q'] = 'index';

// getting prefix path of url
$script = explode('/', $_SERVER['SCRIPT_NAME']);
$prefix = '';
if (count($script)>1){
	unset($script[count($script)-1]);
	$prefix = implode('/', $script);
}

// start magic
include 'core/Page.class.php';
Page::GET_INSTANCE()->run();

?>
