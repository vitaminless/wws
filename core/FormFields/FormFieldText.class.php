<?php

/**
 * FormFieldText.class.php
 * Single text line input
 * @author Benjamin Rösler
 * @version 0.1
 **/

class FormFieldText extends FormField {
	
	public function __construct($name, $desc, $value){
		parent::__construct($name, $desc, $value);
	}
	
}

?>
