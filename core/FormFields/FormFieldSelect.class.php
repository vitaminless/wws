<?php

/**
 * FormFieldSelect.class.php
 * @author Benjamin Rösler
 * @version 0.1
 **/

class FormFieldSelect extends FormField {
	
	private $options = array();
	
	public function __construct($name, $desc, $value){
		parent::__construct($name, $desc, $value);
		$this->options = $desc['options'];
	}
	
	public function html(){
		$this->args['options'] = '';
		foreach ($this->options as $value=>$label)
			$this->args['options'] .= '<option value="' . $value . '" ' . (($this->value==$value)?('selected'):('')) . '>' . $label . '</option>' . PHP_EOL;
		return parent::html();
	}
	
}

?>
