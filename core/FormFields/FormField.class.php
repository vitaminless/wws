<?php

/**
 * FormField.class.php
 * Abstract FormField to implement FormFields
 * @author Benjamin Rösler
 * @version 0.1
 **/

include 'FormFieldHandler.class.php';

class FormField {
	
	protected $args;
	protected $messages = array(
		MSG_ERROR => array(),
		MSG_WARNING => array(),
		MSG_INFO => array(),
		MSG_SUCCESS => array()
	);
	
	protected $name;
	protected $handler = null;
	protected $value = null;
	
	protected $template;
	
	public function __construct($name, $desc, $value){
		$this->name = $name;
		$this->args = array(
			'id' => (isset($desc['id']))?($desc['id']):($name),
			'name' => $name,
			'value' => (!is_null($value))?($value):((isset($desc['value']))?($desc['value']):(null)),
			'label' => (isset($desc['label']))?($desc['label']):(''),
			'hint' => (isset($desc['hint']))?($desc['hint']):(''),
			'description' => (isset($desc['description']))?($desc['description']):(''),
			'args' => (isset($desc['args']))?($desc['args']):(array())
		);
		if (isset($this->args['args']['class'])) $this->args['args']['class'] = array();
		$this->args['args']['class'][] = 'FormField';
		$this->args['args']['class'][] = $desc['type'];
		if (isset($desc['handler'])) $this->handler = $desc['handler'];
		$this->value = $value;
		$this->template = $_SERVER['DOCUMENT_ROOT'] . '/themes/' . Page::GET_INSTANCE()->config['general']['theme'] . '/form/' . $desc['type'] . '.tpl.php';
	}
	
	public function validate(){
		if (is_null($this->handler)) return true;
		else return $this->handler->validate($this, $this->value);
	}
	
	public function addMessage($type, $message){
		$this->messages[$type][] = $message;
		$this->args['args']['class'][] = $type;
	}
	
	public function html(){
		$this->args['messages'] = $this->messages;
		return return_include($this->template, $this->args);
	}
	
}

?>
