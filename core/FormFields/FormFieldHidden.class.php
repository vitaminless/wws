<?php

/**
 * FormFieldHidden.class.php
 * @author Benjamin Rösler
 * @version 0.1
 **/

class FormFieldHidden extends FormField {
	
	public function __construct($name, $desc, $value){
		parent::__construct($name, $desc, $value);
	}
	
}

?>
