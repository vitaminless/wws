<?php

/**
 * FormFieldButton.class.php
 * @author Benjamin Rösler
 * @version 0.1
 **/

class FormFieldButton extends FormField {
	
	public function __construct($name, $desc, $value){
		parent::__construct($name, $desc, $value);
		$this->args['args'] = array_merge($this->args['args'], array('class' => array('FormField', 'FormFieldButton')));
	}
	
}

?>
