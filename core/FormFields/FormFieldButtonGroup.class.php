<?php

/**
 * FormFieldButtonGroup.class.php
 * @author Benjamin Rösler
 * @version 0.1
 **/

class FormFieldButtonGroup extends FormField {
	
	private $buttons = array();
	
	public function __construct($name, $desc, $value){
		parent::__construct($name, $desc, $value);
		foreach ($desc['buttons'] as $name=>$button) $this->addButton($name, $button);
	}
	
	private function addButton($name, $desc){
		$value = (isset($this->value[$name]) && !is_null($this->value[$name]))?($this->value[$name]):(null);
		$name = $this->name . '[' . $name . ']';
		$this->buttons[] = new $desc['type']($name, $desc, $value);
	}
	
	public function html(){
		$this->args['buttons'] = '';
		foreach ($this->buttons as $button) $this->args['buttons'] .= $button->html();
		return parent::html();
	}
	
}

?>
