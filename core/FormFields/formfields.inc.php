<?php

/**
 * formfields.inc.php
 * Including default form fields
 * @author Benjamin Rösler
 * @version 0.1
 **/

// parent class of all form fields
require_once 'FormField.class.php';

// concrete implementation of form fields
require_once 'FormFieldButtonGroup.class.php';
require_once 'FormFieldButton.class.php';
require_once 'FormFieldHidden.class.php';
require_once 'FormFieldSelect.class.php';
require_once 'FormFieldText.class.php';

?>
