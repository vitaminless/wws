<?php

/**
 * Database.class.php
 * Database handler
 * @author Benjamin Rösler
 * @version 0.1
 **/

class Database {
	
	private $connection;
	public $debug = false;
	
	public function __construct($config){
		$dsn = $config['type'] . ':host=' . $config['host'] . ';port=' . $config['port'] . ';dbname=' . $config['db'];
		try {
			$this->connection = new PDO($dsn, $config['user'], $config['pass']);
		} catch (Exception $ex){
			$page = array(
				'type' => 'html',
				'title' => t('Connection failed'),
				'content' => $ex->getMessage()
			);
			Page::GET_INSTANCE()->page500($page);
		}
	}
	
	public function query($table, $fields = array(), $where = null, $order = null, $limit = null){
		$sql = 'SELECT ' . ((empty($fields))?('*'):(implode(', ', $fields))) . ' FROM ' . $table;
		if (!is_null($where)) $sql .= ' WHERE ' . $where;
		if (!is_null($order)) $sql .= ' ORDER BY ' . $order;
		if (!is_null($limit)) $sql .= ' LIMIT ' . $limit;
		$sql .= ';';
		return $this->rawQuery($sql);
	}
	
	public function rawQuery($sql){
		if ($this->debug) Page::$DEBUG .= @d($sql);
		try {
			$stmt = $this->connection->query($sql);
			$stmt->setFetchMode(PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
			return (is_null($result))?(array()):($result);
		} catch (Exception $ex){
			Page::GET_INSTANCE()->addMessage(MSG_ERROR, t('Database error:') . ' ' . $ex->getMessage());
			return array();
		}
	}
	
	public function insert($table, $kvp){
		$kvp = $this->escape($kvp);
		$sql = 'INSERT INTO ' . $table . ' (' . implode(', ', array_keys($kvp)) . ') VALUES (' . implode(', ', array_values($kvp)) . ');';
		if ($this->debug) Page::$DEBUG .= @d($sql);
		try {
			$this->connection->exec($sql);
			return $this->connection->lastInsertId();
		} catch (Exception $ex){
			Page::GET_INSTANCE()->addMessage(MSG_ERROR, t('Database error:') . ' ' . $ex->getMessage());
			return -1;
		}
	}
	
	public function update($table, $kvp, $where = null){
		$values = array();
		foreach ($kvp as $k=>$v){
			$values[] = $k . '=' . $this->escape($v);
		}
		$sql = 'UPDATE ' . $table . ' SET ' . implode(', ', $values);
		if (!is_null($where)) $sql .= ' WHERE ' . $where;
		$sql .= ';';
		if ($this->debug) Page::$DEBUG .= @d($sql);
		$this->connection->exec($sql);
	}
	
	public function remove($table, $where = null){
		$sql = 'DELETE FROM ' . $table;
		if (!is_null($where)) $sql .= ' WHERE ' . $where;
		$sql .= ';';
		if ($this->debug) Page::$DEBUG .= @d($sql);
		$this->connection->exec($sql);
	}
	
	public function escape($val){
		if (is_array($val)){
			foreach ($val as $k=>$v) $val[$k] = $this->escape($v);
			return $val;
		} elseif (is_string($val)){
			return '\'' . str_replace('\'', '\\\'', $val) . '\'';
		} else {
			return $val;
		}
	}
	
}

?>
