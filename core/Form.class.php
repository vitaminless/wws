<?php

/**
 * Form.class.php
 * @author Benjamin Rösler
 * @version 0.1
 **/

define('FORM_METHOD_GET', 'get');
define('FORM_METHOD_POST', 'post');

include 'FormFields/formfields.inc.php';
include 'FormHandler.class.php';

class Form {
	
	private $name;
	private $args;
	private $messages = array(
		MSG_ERROR => array(),
		MSG_WARNING => array(),
		MSG_INFO => array(),
		MSG_SUCCESS => array()
	);
	
	private $values = null;
	private $fields = array();
	
	private $handler = null;
	
	public function __construct($form){
		$this->name = $form['name'];
		$this->args = array(
			'id' => (isset($form['id']))?($form['id']):($form['name']),
			'name' => $form['name'],
			'action' => (isset($form['action']))?($form['action']):($_SERVER['REQUEST_URI']),
			'method' => (isset($form['method']))?($form['method']):(FORM_METHOD_POST),
			'args' => (isset($form['args']))?($form['args']):(array())
		);
		if (isset($form['handler'])) $this->handler = $form['handler'];
		if (isset($_REQUEST[$this->name])) $this->values = $_REQUEST[$this->name];
		foreach ($form['fields'] as $name=>$desc) $this->addField($name, $desc);
		$this->handle();
	}
	
	private function addField($name, $desc){
		$value = (is_null($this->values))?(null):($this->values[$name]);
		$name = $this->name . '[' . $name . ']';
		$this->fields[] = new $desc['type']($name, $desc, $value);
	}
	
	private function handle(){
		if ($this->isSubmitted() && $this->validate() && !is_null($this->handler)){
			$this->handler->execute($this->values);
		}
	}
	
	private function validate(){
		$valid = true;
		foreach ($this->fields as $field){
			if (!$field->validate()) $valid = false;
		}
		if (!$valid) return false;
		
		if (!is_null($this->handler)) return $this->handler->validate($this, $this->values);
		else return true;
	}
	
	public function isSubmitted(){ return !is_null($this->values); }
	
	public function getFields(){ return $this->fields; }
	
	public function getValues(){ return $this->values; }
	
	public function addMessage($type, $message){
		$this->messages[$type][] = $message;
	}
	
	public function html(){
		$this->args['messages'] = $this->messages;
		$this->args['fields'] = '';
		foreach ($this->fields as $field) $this->args['fields'] .= $field->html();
		$tpl = $_SERVER['DOCUMENT_ROOT'] . '/themes/' . Page::GET_INSTANCE()->config['general']['theme'] . '/form/form.tpl.php';
		return return_include($tpl, $this->args);
	}
	
}

?>
