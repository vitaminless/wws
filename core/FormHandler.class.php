<?php

/**
 * FormHandler.class.php
 * Abstract class to handle form validation and execution
 * @author Benjamin Rösler
 * @version 0.1
 **/

class FormHandler {
	
	public function validate($form, $values){}
	
	public function execute($values){}
	
}

?>
