<?php

/**
 * Theme.class.php
 * Render page
 * @author Benjamin Rösler
 * @version 0.1
 **/

define('HTML', 'html');
define('JSON', 'json');

class Theme {
	
	private $args;
	
	public function __construct($args){
		$this->args = $args;
		if ($args['type']==HTML){
			$this->html();
		} elseif ($args['type']==JSON){
			$this->json();
		} else {
			var_dump($this->args);
		}
	}
	
	private function html(){
		$theme = Page::GET_INSTANCE()->config['general']['theme'];
		$this->args['content'] = $this->prepareContent($this->args['content']);
		include 'themes/' . $theme . '/' . $this->args['template'] . '.tpl.php';
	}
	
	private function prepareContent($args){
		if (is_array($args)){
			if (isset($args['template'])){
				return return_include($args['template'], $args['values']);
			} else {
				foreach ($args as $k=>$v){
					if (is_object($v)) $args[$k] = $v->html();
					elseif (is_array($v)) $args[$k] = $this->prepareContent($v);
					else $args[$k] = $v;
				}
				return implode(PHP_EOL, $args);
			}
		} elseif (is_object($args)){
			return $args->html();
		} else {
			return $args;
		}
	}
	
	private function json(){
		echo json_encode($this->args['content']);
	}
	
}

?>
