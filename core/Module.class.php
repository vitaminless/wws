<?php

/**
 * Module.class.php
 * Abstract class to implement a module
 * @author Benjamin Rösler
 * @version 0.1
 **/

class Module {
	
	protected $dir;
	protected $config = array();
	
	public function __construct(){
		$this->config();
	}
	
	private function config(){
		if (is_file($this->dir . '/config.ini'))
			$this->config = parse_ini_file($this->dir . '/config.ini', true);
	}
	
	public function execute($callback){
		$this->$callback();
	}
	
}

?>
