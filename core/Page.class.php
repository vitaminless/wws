<?php

/**
 * Page.class.php
 * This class cares about loading the configuration, the modules and executing the callback.
 * @author Benjamin Rösler
 * @version 0.1
 **/

require_once 'libs/lib.inc.php';
require_once 'functions.php';
require_once 'Database.class.php';
require_once 'Form.class.php';
require_once 'Module.class.php';
require_once 'HtmlTable.class.php';
require_once 'Theme.class.php';

define('MSG_ERROR', 'error');
define('MSG_WARNING', 'warning');
define('MSG_INFO', 'info');
define('MSG_SUCCESS', 'success');

final class Page {
	
	private static $INSTANCE = null;
	public static function GET_INSTANCE(){
		if (is_null(Page::$INSTANCE)) Page::$INSTANCE = new Page();
		return Page::$INSTANCE;
	}
	
	public static $DEBUG = '';
	
	public $messages = array(
		MSG_ERROR => array(),
		MSG_WARNING => array(),
		MSG_INFO => array(),
		MSG_SUCCESS => array()
	);
	public $config = array();
	public $db;
	
	private function __construct(){}
	
	public function run(){
		$this->config();
		$this->execute();
	}
	
	public function addMessage($type, $msg){
		$this->messages[$type][] = $msg;
	}
	
	private function config(){
		$path = 'config/' . $_SERVER['HTTP_HOST'] . '/config.ini';
		if (!is_file($path)) $path = 'config/default/config.ini';
		$this->config = parse_ini_file($path, true);
		$this->db = new Database($this->config['database']);
	}
	
	private function execute(){
		$this->preload();
		$this->load();
		$this->postload();
	}
	
	private function preload(){
		foreach ($this->config['preload'] as $module){
			$this->loadModule($module);
			$module::preload();
		}
	}
	
	private function load(){
		foreach ($this->config['modules']['module'] as $module){
			$system_path = 'core/modules/';
			$user_path = 'modules/';
			if (is_file($system_path . $module . '/paths.ini')){
				$this->url($system_path . $module . '/paths.ini', $module);
			} elseif (is_file($user_path . $module . '/paths.ini')){
				$this->url($user_path . $module . '/paths.ini', $module);
			} else {
				$this->addMessage(MSG_ERROR, sprintf(t('Module %s could not be found!'), $module));
			}
		}
		$page = array(
			'type' => 'html',
			'title' => '',
			'content' => ''
		);
		$this->page404($page);
	}
	
	private function url($ini, $module){
		$paths = parse_ini_file($ini);
		foreach ($paths as $callback=>$path){
			if (preg_match($path, $_REQUEST['q'])){
				$this->loadModule($module);
				$instance = new $module();
				$instance->$callback();
			}
		}
	}
	
	private function postload(){
		foreach ($this->config['preload'] as $module){
			$this->loadModule($module);
			$module::postload();
		}
	}
	
	public function loadModule($module){
		$system_path = 'core/modules/';
		$user_path = 'modules/';
		if (is_dir($system_path . $module)){
			require_once $system_path . $module . '/' . $module . '.class.php';
			return true;
		} elseif (is_dir($user_path . $module)){
			require_once $user_path . $module . '/' . $module . '.class.php';
			return true;
		} else {
			return false;
		}
	}
	
	public function page200($args){
		header('HTTP/1.0 200 OK');
		if (!isset($args['template'])) $args['template'] = 'page';
		$this->render($args);
	}
	
	public function page403($args){
		header('HTTP/1.0 403 Forbidden');
		if (!isset($args['template'])) $args['template'] = '403';
		$this->render($args);
	}
	
	public function page404($args){
		header('HTTP/1.0 Not Found');
		if (!isset($args['template'])) $args['template'] = '404';
		$this->render($args);
	}
	
	public function page500($args){
		header('HTTP/1.0 Internal Server Error');
		if (!isset($args['template'])) $args['template'] = '500';
		$this->render($args);
	}
	
	private function render($args){
		$args['name'] = $this->config['general']['name'];
		$args['messages'] = $this->messages;
		new Theme($args);
		die();
	}
	
	public function redirect($url){
		header('Location: ' . $url);
		die();
	}
	
}

?>
