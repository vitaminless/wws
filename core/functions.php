<?php

/**
 * functions.php
 * Several useful functions
 * @author Benjamin Rösler
 * @version 0.1
 **/

function return_include($file, $args){
	ob_start();
	include $file;
	return ob_get_clean();
}

function t($str){
	return $str;
}

function html_args($args){
	$result = array();
	foreach ($args as $k=>$v){
		if (!is_array($v)) $result[] = $k . '="' . $v . '"';
		else $result[] = $k . '="' . implode(' ', $v) . '"';
	}
	return implode(' ', $result);
}

function l($label, $href, $args=array()){
	if (strpos($href, '://')===false){
		global $prefix;
		$href = $prefix . $href;
	}
	$args['href'] = $href;
	return '<a ' . html_args($args) . '>' . $label . '</a>';
}

?>
