<?php

/**
 * HtmlTable.class.php
 * @author Benjamin Rösler
 * @version 0.1
 **/

class HtmlTable {
	
	private $thead = null;
	private $tbody = null;
	private $tfoot = null;
	private $args = array();
	
	public function __construct($table){
		$this->tbody = new HtmlTableBody($table['body']);
		if (isset($table['head'])) $this->thead = new HtmlTableHead($table['head']);
		if (isset($table['foot'])) $this->tfoot = new HtmlTableFoot($table['foot']);
		if (isset($table['args'])) $this->args = $table['args'];
	}
	
	public function html(){
		$html = '<table ' . html_args($this->args) . '>' . PHP_EOL;
		if (!is_null($this->thead)) $html .= $this->thead->html();
		$html .= $this->tbody->html();
		if (!is_null($this->tfoot)) $html .= $this->tfoot->html();
		$html .= '</table>' . PHP_EOL;
		return $html;
	}
	
}

class HtmlTableHead {
	
	private $row;
	private $args = array();
	
	public function __construct($head){
		$this->row = new HtmlTableHeadRow($head['row']);
		if (isset($head['args'])) $this->args = $head['args'];
	}
	
	public function html(){
		$html = '<thead ' . html_args($this->args) . '>' . PHP_EOL;
		$html .= $this->row->html();
		$html .= '</thead>' . PHP_EOL;
		return $html;
	}
	
}

class HtmlTableFoot {
	
	private $row;
	private $args = array();
	
	public function __construct($head){
		$this->row = new HtmlTableHeadRow($head['row']);
		if (isset($head['args'])) $this->args = $head['args'];
	}
	
	public function html(){
		$html = '<tfoot ' . html_args($this->args) . '>' . PHP_EOL;
		$html .= $this->row->html();
		$html .= '</tfoot>' . PHP_EOL;
		return $html;
	}
	
}

class HtmlTableHeadRow {
	
	private $cells = array();
	private $args = array();
	
	public function __construct($row){
		foreach ($row['cells'] as $cell) $this->cells[] = new HtmlTableHeadCell($cell);
		if (isset($row['args'])) $this->args = $row['args'];
	}
	
	public function html(){
		$html = '<tr ' . html_args($this->args) . '>' . PHP_EOL;
		foreach ($this->cells as $cell) $html .= $cell->html() . PHP_EOL;
		$html .= '</tr>' . PHP_EOL;
		return $html;
	}
	
}

class HtmlTableHeadCell {
	
	private $value;
	private $args = array();
	
	public function __construct($cell){
		$this->value = $cell['value'];
		if (isset($cell['args'])) $this->args = $cell['args'];
	}
	
	public function html(){
		return '<th ' . html_args($this->args) . '>' . $this->value . '</th>';
	}
	
}

class HtmlTableBody {
	
	private $rows = array();
	private $args = array();
	
	public function __construct($body){
		foreach ($body['rows'] as $row) $this->rows[] = new HtmlTableRow($row);
		if (isset($body['args'])) $this->args = $body['args'];
	}
	
	public function html(){
		$html = '<tbody ' . html_args($this->args) . '>' . PHP_EOL;
		foreach ($this->rows as $row) $html .= $row->html();
		$html .= '</tbody>' . PHP_EOL;
		return $html;
	}
	
}

class HtmlTableRow {
	
	private $cells = array();
	private $args = array();
	
	public function __construct($row){
		foreach ($row['cells'] as $cell) $this->cells[] = new HtmlTableCell($cell);
		if (isset($row['args'])) $this->args = $row['args'];
	}
	
	public function html(){
		$html = '<tr ' . html_args($this->args) . '>' . PHP_EOL;
		foreach ($this->cells as $cell) $html .= $cell->html();
		$html .= '</tr>' . PHP_EOL;
		return $html;
	}
	
}

class HtmlTableCell {
	
	private $value;
	private $args = array();
	
	public function __construct($cell){
		$this->value = $cell['value'];
		if (isset($cell['args'])) $this->args = $cell['args'];
	}
	
	public function html(){
		return '<td ' . html_args($this->args) . '>' . $this->value . '</td>' . PHP_EOL;
	}
	
}

?>
